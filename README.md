# cooking with python

## Description

Made this library for making cooking recipes.
This is just for fun

## Example of use

using with statement:

```
from Recipe import Recipe

# haciendo agua de jamaica
with Recipe("agua de jamaica") as agua:

    # agrega ingredientes
    agua.add_ingredient("agua", "5", "lts")
    agua.add_ingredient("jamaica", "1/4", "kg")
    agua.add_ingredient("azucar", "3", "cucharones")

    # escribir instrucciones
    agua.add_inst("agregar agua a una cubeta")
    agua.add_inst("agregar azucar a una cubeta")
    agua.add_inst("calentar la jamaica en la estufa")
    agua.add_inst("cuando esté lista, dejar enfriar")
    agua.add_inst("colar el líquido en la cubeta")
    agua.add_inst("batir")

    awa = agua

print(str(agua))
```

using operators:

```
agua = Recipe("agua de jamaica")
print(str(agua))

# agrega ingredientes
agua << ("agua", "5", "lts")
agua << ("jamaica", "1/4", "kg")
agua << ("azucar", "3", "cucharones")

# escribe instrucciones
agua >> "agregar agua a una cubeta"
agua >> "agregar azucar a una cubeta"
agua >> "calentar la jamaica en la estufa"
agua >> "cuando esté lista, dejar enfriar"
agua >> "colar el líquido en la cubeta"
agua >> "batir"

print(agua)
```
output:

```
added agua 5 lts
added jamaica 1/4 kg
added azucar 3 cucharones

# agua de jamaica
ingredients:
 * 5 lts of agua
 * 1/4 kg of jamaica
 * 3 cucharones of azucar
instructions:
1.- agregar agua a una cubeta
2.- agregar azucar a una cubeta
3.- calentar la jamaica en la estufa
4.- cuando esté lista, dejar enfriar
5.- colar el líquido en la cubeta
6.- batir
```


## TODO 
 
- allow multiple sintaxis, just the simplest one and the
- allow multiplying the ingredients for adjusting the portions
- allow exporting to pdf or markdown
- read more cooking recipes to check recipes patterns
- allow user friendly input with regex maybe
- add Exceptions

