class Recipe(object):

    def __init__(self, name="Untitled"):
        self.ingredients = {}
        self.name = name
        self.instructions = []

    def __str__(self):
        return self. __repr__()

    def __repr__(self):
        val = "# {}\n".format(self.name)
        val += "ingredients:\n"
        for ing in self.ingredients.keys():
            val += " * {} {} of {}\n".format(
                self.ingredients[ing][0], self.ingredients[ing][1], ing)
        val += "instructions:\n"
        for n, inst in enumerate(self.instructions):
            val += "{}.- {}\n".format(n+1, inst)
        return val

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def add_ingredient(self, name=None, portion=None, units=None, **kargs):
        if kargs != {}:
            print("added", kargs[name], kargs[portion], kargs[units])
            self.ingredients.update(
                {kargs["name"]:  [kargs["portion"], kargs["units"]]})
            return {kargs["name"]: [kargs["portion"], kargs["units"]]}
        elif type(name) != tuple:
            print("added", name, portion, units)
            self.ingredients.update({name: [portion, units]})
            return {name: [portion, units]}
        else:
            print("added", name[0], name[1], name[2])
            self.ingredients.update({name[0]: [name[1], name[2]]})
            return {name[0]: [name[1], name[2]]}

    def add_ingredients(self, ingredients):
        for ing in ingredients.keys():
            if len(ingredients[ing]) != 2:
                print("error in ingredient")
        self.ingredients.update(ingredients)

    def add_inst(self, instruction):
        self.instructions.append(instruction)

    def __lshift__(self, ingredient):
        self.add_ingredient(ingredient)

    def __rshift__(self, instruction):
        self.add_inst(instruction)


class hola(object):

    def __init__(self, k):
        self.name = k()

    def __call__(self):
        print("aaeo")
