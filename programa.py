from Recipe import Recipe


# using with statement
# haciendo agua de jamaica
with Recipe("agua de jamaica") as agua:

    # agrega ingredientes
    agua.add_ingredient("agua", "5", "lts")
    agua.add_ingredient("jamaica", "1/4", "kg")
    agua.add_ingredient("azucar", "3", "cucharones")

    # escribir instrucciones
    agua.add_inst("agregar agua a una cubeta")
    agua.add_inst("agregar azucar a una cubeta")
    agua.add_inst("calentar la jamaica en la estufa")
    agua.add_inst("cuando esté lista, dejar enfriar")
    agua.add_inst("colar el líquido en la cubeta")
    agua.add_inst("batir")

    awa = agua

print(str(agua))


# using operators
agua = Recipe("agua de jamaica")

# agrega ingredientes
agua << ("agua", "5", "lts")
agua << ("jamaica", "1/4", "kg")
agua << ("azucar", "3", "cucharones")

# escribe instrucciones
agua >> "agregar agua a una cubeta"
agua >> "agregar azucar a una cubeta"
agua >> "calentar la jamaica en la estufa"
agua >> "cuando esté lista, dejar enfriar"
agua >> "colar el líquido en la cubeta"
agua >> "batir"

print(str(agua))


# using object notation
agua = Recipe("agua de jamaica")

# agrega ingredientes
agua.add_ingredient("agua", "5", "lts")
agua.add_ingredient("jamaica", "1/4", "kg")
agua.add_ingredient("azucar", "3", "cucharones")

# escribir instrucciones
agua.add_inst("agregar agua a una cubeta")
agua.add_inst("agregar azucar a una cubeta")
agua.add_inst("calentar la jamaica en la estufa")
agua.add_inst("cuando esté lista, dejar enfriar")
agua.add_inst("colar el líquido en la cubeta")
agua.add_inst("batir")

print(agua)
